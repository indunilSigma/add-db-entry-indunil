const AWS = require('aws-sdk');
const ses = new AWS.SES();

exports.handler = async (event) => {

    let tabledetails = event.Records[0].dynamodb;
    console.log(tabledetails);

    let name = tabledetails.NewImage.Name.S;
    let email = tabledetails.NewImage.Email.S;
    let feedback = tabledetails.NewImage.Feedback.S;
    let messagebody = 'Hi' + ' ' + name + '! Thank you for your feedback'

    try {
        let data = await ses.sendEmail({
            Source: "indunil+34@adroitlogic.com",
            Destination: {
                ToAddresses: ['indunil@adroitlogic.com']
            },
            Message: {
                Subject: {
                    Data: "hii thank you for your feeddback!"
                },
                Body: {
                    Text: {
                        Data: messagebody
                    }
                }
            }
        }).promise();

    } catch (err) {
        console.log("Email sending failed", err)
        // error handling goes here
    };

    return { "message": "Successfully executed" };
};